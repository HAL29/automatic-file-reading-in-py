import os
import datetime
import modin.pandas as pd
import json
import featuretools as ft
import matplotlib.pyplot as plt, mpld3
import numpy as np
import time
import matplotlib as mpl
import seaborn as sns
from prophet import Prophet
from prophet.plot import add_changepoints_to_plot
from prophet.serialize import model_to_json, model_from_json
from pandas_profiling import ProfileReport
from distributed import Client
from dateutil.parser import parse 

plt.rcParams.update({'figure.figsize': (10, 7), 'figure.dpi': 120})


#start timer for code execution

start = time.time()

def sqlread(con_w_sql, sep='#'):
    con, sql = special_path.split(sep)
    df = pd.read_sql(sql, con)
    return df.head()

readict = {
    ".csv": {
        "read": pd.read_csv
    },
    ".tsv": {
        "read": pd.read_csv 
    },
    ".json": {
        "read": pd.read_json
    },
    ".xlsx": {
        "read": pd.read_excel 
    },
    ".xml": {
        "read": pd.read_xml 
    },
    ".xls": {
        "read": pd.read_excel 
    },
    ".hdf": {
        "read": pd.read_hdf 
    },
    ".sql": {
        "read": sqlread
    }
        
}

#data reading with Modin and Dtypes with feature tools

def read_any(file):
    _, ext = os.path.splitext(file)
    
    #Start timer for file reading
    
    tic = time.perf_counter()
    df = readict[ext]["read"](file)
    
    #End timer for file reading
    
    toc = time.perf_counter()
    readt = (f"File read in {toc - tic:0.4f} seconds")
    es = ft.EntitySet()
    es = es.entity_from_dataframe(entity_id="df",
                                  dataframe = df)
    head = df.head(100).values.tolist()
    dtype = es["df"]
    
    dtypedict = dtype.variable_types
    types = []
    
    for value in dtypedict.values():
        types.append(value.type_string)

#identifying date cols and turning them to datetime type

    for col in df.columns:
        if df[col].dtype == 'object':
            try:
                df[col] = pd.to_datetime(df[col])
            except ValueError:
                pass


#Prophet

#selecting the DS and Y columns

    df["ds"] = df.select_dtypes(include='datetime64')
    ycol = input("enter the column name you want to use : ")
    df["y"] = df[ycol]
    
    m = Prophet()
    m.fit(df)
    
#saving model in json format
    
    with open('serialized_model.json', 'w') as fout:
        json.dump(model_to_json(m), fout) 
    
#making future df

    future = m.make_future_dataframe(periods=365)
    future.tail()
    
#Prophet Forecast
    
    forecast = m.predict(future)
    forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()

#Forecast Plot

    fig1 = m.plot(forecast)
    
#defining the wanted keys

    keys = ['axes', 'data']
    
#transforming the plot to a dict

    fig1dict = mpld3.fig_to_dict(fig1)
    
    fig1dictjs = {x:fig1dict[x] for x in keys}
    
#formatting the dict to json

    fig1js = json.dumps(fig1dictjs)
    
#Plot Components

    fig2 = m.plot_components(forecast)

#transforming the plot to a dict
    
    fig2dict = mpld3.fig_to_dict(fig2)
    
    fig2dictjs = {x:fig2dict[x] for x in keys}
    
#formatting the dict to json

    fig2js = json.dumps(fig2dictjs)

#Trend Changepoint
    
    fig3 = m.plot(forecast)
    a = add_changepoints_to_plot(fig3.gca(), m, forecast)

#transforming the plot to a dict

    fig3dict = mpld3.fig_to_dict(fig3)
    
    fig3dictjs = {x:fig3dict[x] for x in keys}

#formatting the dict to json

    fig3js = json.dumps(fig3dictjs)
    
#pandas profiling
    
    pddf = df._to_pandas() 
    profile = ProfileReport(pddf, title="Pandas Profiling Report")
    profile.to_json()
    profile.to_file("your_report.json")
    
#end timer for code execution 
    
    end = time.time()
    timer = (f"Runtime of the program is {end - start}")
    
#result return
    
    return json.dumps(head), json.dumps(types), readt, timer
file = input("enter the path to the file you want to open : ")
read_any(file)


