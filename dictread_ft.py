import os
import modin.pandas as pd
import json
import featuretools as ft
import matplotlib.pyplot as plt
import numpy as np
import time
from pandas_profiling import ProfileReport
from distributed import Client


#start timer for code execution

start = time.time()

def sqlread(con_w_sql, sep='#'):
    con, sql = special_path.split(sep)
    df = pd.read_sql(sql, con)
    return df.head()

readict = {
    ".csv": {
        "read": pd.read_csv
    },
    ".tsv": {
        "read": pd.read_csv 
    },
    ".json": {
        "read": pd.read_json
    },
    ".xlsx": {
        "read": pd.read_excel 
    },
    ".xml": {
        "read": pd.read_xml 
    },
    ".xls": {
        "read": pd.read_excel 
    },
    ".hdf": {
        "read": pd.read_hdf 
    },
    ".sql": {
        "read": sqlread
    }
        
}

#data reading with Modin and Dtypes with feature tools

def read_any(file):
    _, ext = os.path.splitext(file)
    
    #Start timer for file reading
    
    tic = time.perf_counter()
    df = readict[ext]["read"](file)
    
    #End timer for file reading
    
    toc = time.perf_counter()
    readt = (f"File read in {toc - tic:0.4f} seconds")
    es = ft.EntitySet()
    es = es.entity_from_dataframe(entity_id="df",
                                  dataframe = df)
    head = df.head(100).values.tolist()
    dtype = es["df"]
    
    dtypedict = dtype.variable_types
    types = []
    
    for value in dtypedict.values():
        types.append(value.type_string)
    
    #data visualisation 
    
    viz = []
    numericdata = []
    for x in df.columns :
        if(df[x].dtype == np.float64 or df[x].dtype == np.int64):
            
                numericdata.append(df[x])
                
                g = plt.hist(df[x])               
                viz.append((x,g))
                
                lfl = []
                lsl = []
                titles = []
                
                for x in range(len(viz)): 
                    title = viz[x][0]
                    fl = viz[x][1][0]
                    sl = viz[x][1][1]
                    
                    titles.append(title)
                    lfl.append(list(fl))
                    lsl.append(list(sl))
                    
                    vizdict = [{'title': title, 'first array': fa, 'second array': sa} for title,fa,sa in zip(titles,lfl,lsl)] 

    #pandas profiling
    
    pddf = df._to_pandas() 
    profile = ProfileReport(pddf, title="Pandas Profiling Report")
    profile.to_json()
    profile.to_file("your_report.json")
    
    #end timer for code execution 
    
    end = time.time()
    timer = (f"Runtime of the program is {end - start}")
    
    return json.dumps(head), json.dumps(types), readt, timer, json.dumps(vizdict)
file = input("enter the path to the file you want to open : ")
read_any(file)







