import os
import pandas as pd
import json
import lux
import numpy as np
from lux.vis.Vis import Vis

def sqlread(con_w_sql, sep='#'):
    con, sql = special_path.split(sep)
    df = pd.read_sql(sql, con)
    return df.head()

readict = {
    ".csv": {
        "read": pd.read_csv
    },
    ".tsv": {
        "read": pd.read_csv 
    },
    ".json": {
        "read": pd.read_json
    },
    ".xlsx": {
        "read": pd.read_excel 
    },
    ".xls": {
        "read": pd.read_excel 
    },
    ".hdf": {
        "read": pd.read_hdf 
    },
    ".sql": {
        "read": sqlread
    }
        
}
def visualise(file):
    _, ext = os.path.splitext(file)
    df = readict[ext]["read"](file)
    
    allvis = []
    for column in df : 
        intent = [column]
        vis = Vis(intent,df)
        vega = vis.to_vegalite(prettyOutput=False)
        wanted_keys = set(['config', 'data', 'mark', 'encoding', 'height', 'width', "$schema"])
        cleandict = {x: vega[x] for x in vega.keys() & wanted_keys}
            
    return json.dumps(cleandict)

file = input("enter the path to the file you want to open : ")
visualise(file)
